public class homework07 {
    public static void main(String[] args) {
        int[] array = getArray();
        System.out.println(toString(array));
        System.out.println(arrayMin(array));
        System.out.println(arrayMax(array));
    }

    public static int arrayMax(int[] array) {
        int max;
        if (array.length > 0) {
            max = array[0];
            for (int elem : array) {
                max = Math.max(elem, max);
            }
            return max;
        }
        throw new RuntimeException();
    }

    public static int arrayMin(int[] array) {
        int min;
        if (array.length > 0) {
            min = array[0];
            for (int elem : array) {
                min = Math.min(elem, min);
            }
            return min;
        }
        throw new RuntimeException();
    }

    public static String toString(int[] array) {
        StringBuilder answer = new StringBuilder("[");
        for (int elem : array) {
            answer.append(elem);
            answer.append(", ");
        }
        answer.append("]");
        return answer.toString();
    }


    public static int[] getArray() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите кол-во элементов - ");
        int arrayLength = scanner.nextInt();
        int[] array = new int[arrayLength];
        for (int i = 0; i < array.length; i++) {
            System.out.print("Введите элемент - ");
            array[i] = scanner.nextInt();
        }
        return array;
    }
}



